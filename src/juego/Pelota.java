package juego;

import java.awt.Color;
import entorno.Entorno;

public class Pelota {
	// VARIABLES DE INSTANCIA
	private double x;
	private double y;
	private double diametro;
	private double angulo;
	private Color color;
	private double velocidad;

	public Pelota(double x, double y) {
		this.x = x;
		this.y = y;
		this.color = Color.cyan;
		this.diametro = 50;
		this.angulo = 2;
		this.velocidad = 1;
	}

	public void dibujar(Entorno e) {
		e.dibujarCirculo(this.x, this.y, this.diametro, this.color);
	}

	public void avanzar() {
		this.x += Math.cos(this.angulo) * this.velocidad;
		this.y += Math.sin(this.angulo) * this.velocidad;
	}

	public void aumentarVelocidad() {
		this.velocidad += 0.5;
	}

	public boolean chocaConEntorno(Entorno e) {
		if (x - diametro / 2 < 0 || x + diametro / 2 > e.ancho()) {
			return true;
		}
		if (y - diametro / 2 < 0 || y + diametro / 2 > e.alto()) {
			return true;
		}
		return false;
	}

	public void rebotar(Entorno e) {
		if (y - diametro / 2 < 0 ) { // agregar || y + diametro / 2 > e.alto() para rebotar abajo
			this.angulo = this.angulo * -1;
		}
		if (x - diametro / 2 < 0 || x + diametro / 2 > e.ancho()) {
			this.angulo = Math.PI - this.angulo;
		}
	}
	
	public boolean chocaConBarra(Barra b) {
		return (b.getX() - b.getAncho()/2 < x + diametro/2 && 
				x - diametro/2 < b.getX() + b.getAncho()/2 &&
				
				b.getY() - b.getAlto()/2 < y + diametro/2 &&
				y - diametro/2 < b.getY() + b.getAlto()/2);
	}
	
	public void rebotarConBarra() {
		this.angulo = this.angulo * -1;
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
