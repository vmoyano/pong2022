package juego;

import java.awt.Color;

import entorno.Entorno;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Pelota pelota;
	private Barra barra;
	
	
	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Pong - V0.01", 800, 600);
		
		// Inicializar lo que haga falta para el juego
		this.pelota = new Pelota(entorno.ancho()/2, 200);
		this.barra = new Barra(entorno.ancho()/2, entorno.alto()-20);
		

		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick() {
		// Procesamiento de un instante de tiempo
		// ...
	
		pelota.dibujar(entorno);
		barra.dibujar(entorno);
		
		pelota.avanzar();
		
		if(pelota.chocaConEntorno(entorno)) {
			pelota.rebotar(entorno);
		}
		
		if (entorno.sePresiono('s')) {
			pelota.aumentarVelocidad();
		}
		
		if (entorno.estaPresionada(entorno.TECLA_DERECHA)) {
			barra.moverDerecha(entorno);
		}
		
		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
			barra.moverIzquierda();
		}
		
		if(pelota.chocaConBarra(barra)) {
			pelota.rebotarConBarra();
		}
		
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
